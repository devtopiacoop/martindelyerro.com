<?php
/**
 * @file
 * Theme setting callbacks for the icp_theme.
 */

// Impliments hook_form_system_theme_settings_alter().
function icp_theme_form_system_theme_settings_alter(&$form, $form_state) {
	$form['theme_settings']['active_responsive'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Enable Custom Responsive'),
	    '#default_value' => theme_get_setting('active_responsive'),
	);
	$form['theme_settings']['show_grid'] = array(
	    '#type' => 'checkbox',
	    '#title' => t('Show Grid (for development use)'),
	    '#default_value' => theme_get_setting('show_grid'),
	);
}
