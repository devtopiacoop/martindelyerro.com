(function ($) {
  
// GLOBALS
Drupal.appConf = {};
Drupal.queryMedia = {};


// FUNCTIONS AND DRUPAL METHODS
// ----------------------------------------------------------------------------------------------------

Drupal.queryMedia.get_width = function () {
	return $(window).width();
};

Drupal.queryMedia.get_name = function () {
	var w = this.get_width();
	if (w <= 320) {
		return "IPHONE3";
	} else if (w <= 480) {
		return "IPHONE3-LANDSCAPE";
	} else if (w <= 640) {
		return "IPHONE4";
	} else if (w <= 767) {
		return "IPHONE4";
	} else if (w <= 768) {
		return "IPAD";
	} else if (w <= 1024) {
		return "IPAD-LANDSCAPE";
	} else if (w <= 1280) {
		return "NOTEBOOK";
	} else {
		return "COMPUTER";
	}
};

Drupal.queryMedia.get_group = function () {
  var n = this.get_name();
  if (n == "IPHONE3" || n == "IPHONE3-LANDSCAPE" || n == "IPHONE4" || n == "IPHONE4-LANDSCAPE") {
    return "MOBILE";
  } else if (n == "IPAD" || n == "IPAD-LANDSCAPE") {
    return "TABLET";
  } else if (n == "NOTEBOOK") {
    return "NOTEBOOK"
  } else {
    return "COMPUTER"
  }
};

Drupal.queryMedia.get_so = function () {
    var ua = navigator.userAgent;
    if(ua.match(/(iPhone|iPod|iPad)/)){
      return "IOS";
    } else if(ua.match(/BlackBerry/)){
      return "BLACKBERRY";
    } else if(ua.match(/Android/)){
      return "ANDROID";
    } else {
      return "OTHER";
    }
};

Drupal.initStart = function(){
  console.log("Ignition Sequence Start...");
  console.log(Drupal);

   /* Scroll top */
   $('a.back-top').click(function(){$('html, body').animate({scrollTop:'0px'},600);return false});
};

Drupal.initResponsive = function(){
 if(Drupal.settings.activeResponsive == '1'){
    Drupal.appConf.ADAPT_CONFIG = {
      path: Drupal.settings.basePath + Drupal.settings.pathToTheme + '/css/',
      dynamic: true,
      //callback: myCallback,
      range: [
        '0px    to 760px  = global_mobile.css',
        '760px  to 980px  = global_tablet.css',
        '980px            = global.css'
      ]
    };

    addOrientationBodyClass();

    $(window).resize(function() {
      setTimeout("resize()",500);
    });    
    $(window).bind('orientationchange', function(){
      addOrientationBodyClass();
      resize();
    });
    $(window).scroll(function() {
      if(Drupal.queryMedia.get_so() == "ANDROID"){
        $("#wrap").css('top',$('body').scrollTop());
      }
    });

    //Responsive ToggleNav
    //********************
    $('<a href="#" class="togglenav">Toggle Nav</a>').prependTo('#zone-user-wrapper .region-inner');
    $('a.togglenav').click(function(e){
      e.preventDefault();
      $('body').scrollTop(0);
      $('html').scrollTop(0);
      
      //if(Drupal.queryMedia.get_group() == "MOBILE" || Drupal.queryMedia.get_group() == "TABLET"){
      if($('body').hasClass('open-responsive')){
        $('body').removeClass('open-responsive');
      
        if(Drupal.queryMedia.get_so() == "ANDROID"){
          $("#wrap").css('width','auto');
          $("#wrap").css('height','auto');
          $("#wrap").css('position','initial');
        } else {
          $("#page").css('position','absolute'); 
        }
      } else {
        $('body').addClass('open-responsive');

        if(Drupal.queryMedia.get_so() == "ANDROID"){
          $("#wrap").width($(window).width());
          $("#wrap").height($(window).height()+30);
          $("#wrap").css('position','absolute');
        } else {
          $("#page").css('position','fixed'); 
        }
      }
      //}
    });

    //Responsive SideMenu
    //*******************
    // Initial State
    $('#responsive-panel > ul').addClass('mainresponsive');
    $('#responsive-panel ul.responsive li ul.responsive li ul.responsive li ul.responsive').slideUp(0); //4 Level
    $('#responsive-panel ul.responsive li ul.responsive li ul.responsive').slideUp(0); //3 Level
    $('#responsive-panel ul.responsive li ul.responsive').slideUp(0); //2 Level
    $('#responsive-panel ul.responsive li ul.responsive').parent().addClass('level_2'); //2 Level
    $('#responsive-panel ul.responsive li ul.responsive li ul.responsive').parent().removeClass('level_2').addClass('level_3'); //3 Level
    $('#responsive-panel ul.responsive li ul.responsive li ul.responsive li ul.responsive').parent().removeClass('level_3').addClass('level_4'); //4 Level
    
    // Open/Closed/Link controllers
    $('#responsive-panel a').each(function(i,e){
      var n = $(e).next();
      if(n.length != 0) {
        $(e).parent().addClass('closed');
        $(e).addClass('closed');
      } else {
        $(e).addClass('link');
      }
    });

    // Open menu selected
    $('#responsive-panel li.active-trail > ul.responsive').slideDown(0);
    $('#responsive-panel li.active-trail.closed > a.closed').removeClass('closed').addClass('open');
    $('#responsive-panel li.active-trail.closed').removeClass('closed').addClass('open');

    $('#responsive-panel a').click(function(e){
      //if(Drupal.queryMedia.get_group() == "MOBILE" || Drupal.queryMedia.get_group() == "TABLET"){
        e.preventDefault();
        
        if($(this).attr('class').indexOf('link') == -1){
          var obj = $(this).next();
          var lvl = 'level_' + obj.parent().attr('class').split('level_')[1].split(" ")[0];

          if($(this).hasClass('closed')){
            $('#responsive-panel .'+lvl+'.open > ul.responsive').slideUp();
            $('#responsive-panel .'+lvl+'.open > a.open').removeClass('open').addClass('closed');
            $('#responsive-panel .'+lvl+'.open').removeClass('open').addClass('closed');

            obj.slideDown();
            $(this).removeClass('closed').addClass('open');
            $(this).parent().removeClass('closed').addClass('open');
          } else {
            obj.slideUp();
            $(this).removeClass('open').addClass('closed');
            $(this).parent().removeClass('open').addClass('closed');
          }
        } else {
          window.location = $(this).attr('href');
        }
      //}
    })

    // Secondary responsive only 3 items
    var show = 2;
    $('#responsive-panel .secondary-responsive li').each(function(e,n){
      if(e<=show){
        $(this).addClass('on');
      } else {
        $(this).addClass('off');
      }
    });
  
    adaptJS(window, window.document, Drupal.appConf.ADAPT_CONFIG);
 }

 setTimeout("resize()",500);
};

Drupal.manipulateDrupalHTML = function(){
  //:last-child
  $(':last-child').addClass('lastchild');

  if($('h6.site-slogan span').length <= 0) {
    // Page Title
    var tit = $('head title').text();
    tit = tit.replace('[',''); 
    tit = tit.replace(']',''); 
    tit = tit.replace('#',' | '); 
    tit = tit.replace('/',' | '); 

    $('head title').text(tit);

    // Header slogan
    var h = $('h6.site-slogan').text();
    var t = '';

    if(h.indexOf('[')!=-1 && h.indexOf(']')!=-1){
      var t = $('h6.site-slogan').text().split('[')[1];
      t = t.split(']')[0];
    }

    if(h.indexOf('#')!=-1){
      h = h.split('#')[0]+'<span>'+h.split('#')[1]+'</span>';
    }
    
    h = h.replace('/','<br />');    
    h = h.replace('['+t+']','<a href="tel:'+t+'">'+t+'</a>');    

    $('h6.site-slogan').html(h)
  }


  // Home - 2 columns
  if(Drupal.queryMedia.get_group() != "MOBILE"){
  setTimeout(function(){
    var h_2cols = ($('body.front .region-preface-second #block-views-destacados-block .block-inner').height()>=$('body.front .region-preface-third #block-views-noticias-block .block-inner').height())?$('body.front .region-preface-second #block-views-destacados-block .block-inner').height():$('body.front .region-preface-third #block-views-noticias-block .block-inner').height();
    $('body.front .region-preface-second #block-views-destacados-block .block-inner').height(h_2cols);
    $('body.front .region-preface-third #block-views-noticias-block .block-inner').height(h_2cols);
  },600);
  }
};

Drupal.cookiesMessage = function(){
  if (!$.cookie("msgcookie")) {
    $("body div#page.page").prepend("<div class='msgcookie'><p><a href='#' class='close'>cerrar mensaje</a>Esta web utiliza 'cookies' para ofrecerle una mejor experiencia y servicio. Al navegar o utilizar nuestros servicios usted acepta el uso que hacemos de las 'cookies'.  <a href='/politica-de-privacidad'>M&aacute;s informaci&oacute;n</a></p></div>");

    $('.msgcookie a.close').click(function(e){
      e.preventDefault();
      $.cookie('msgcookie', 'ok');
      $(".msgcookie").fadeOut();
    });

    $(window).scroll(function (e) {
      if($('body.open-responsive').length <= 0){
        var minopacity = 0.9;
        var maxopacity = 1;
        var s = $(document).scrollTop();

        //console.log(s);
        if(s<=10){
          $('.msgcookie').css('opacity',maxopacity);
          $('.msgcookie').css('position','relative');
        } else {
          $('.msgcookie').css('opacity',minopacity);
          $('.msgcookie').css('position','fixed');        
        }        
      }
    });
  } 
}

// EXTRA FUNCTIONS
// ----------------------------------------------------------------------------------------------------
resize = function(){
  // Home - 2 columns
  if(Drupal.queryMedia.get_group() != "MOBILE"){
  setTimeout(function(){
    var h_2cols = ($('body.front .region-preface-second #block-views-destacados-block .block-inner').height()>=$('body.front .region-preface-third #block-views-noticias-block .block-inner').height())?$('body.front .region-preface-second #block-views-destacados-block .block-inner').height():$('body.front .region-preface-third #block-views-noticias-block .block-inner').height();
    $('body.front .region-preface-second #block-views-destacados-block .block-inner').height(h_2cols);
    $('body.front .region-preface-third #block-views-noticias-block .block-inner').height(h_2cols);
  },600);
  }
};

addOrientationBodyClass = function(){
  $('body').removeClass('orientation-landscape');
  $('body').removeClass('orientation-portrait');

  if(Drupal.queryMedia.get_group() == "MOBILE" || Drupal.queryMedia.get_group() == "TABLET"){
    var n = Drupal.queryMedia.get_name();
    n = n.split("-")[1];

    if(n == "LANDSCAPE") {
      $('body').addClass('orientation-landscape');
    } else {
      $('body').addClass('orientation-portrait');
    }

    if(Drupal.queryMedia.get_so() == "ANDROID" && $("#wrap").css('position') == 'absolute'){
      $('body').scrollTop(0);
      $('html').scrollTop(0);

      setTimeout(function(){
        //alert($(window).width() + "x" + $(window).height());
        $("#wrap").width($(window).width());
        $("#wrap").height($(window).height()-10);
      },1000);
    }
  }
}


// DRUPAL BEHAVIORS
// ----------------------------------------------------------------------------------------------------

// Init
Drupal.behaviors.icp_theme = {
  attach: function(context, settings) {
    Drupal.initStart();
    Drupal.initResponsive();
    Drupal.manipulateDrupalHTML();
    Drupal.cookiesMessage();
  }
};
  
})(jQuery);

