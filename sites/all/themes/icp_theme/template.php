<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
 
function ice_theme_view($node, $view_mode) {
 	var_dump($node);
  if ($view_mode == 'full' && node_is_page($node)) {
    $content['group_tratamiento_tabs']['group_tratamiento_tab_galeria']['field_gallery']['#access'] = false; 
  var_dump($content['group_tratamiento_tabs']['group_tratamiento_tab_galeria']['field_gallery']);  
  if (count($content['group_tratamiento_tabs']['group_tratamiento_tab_galeria']['field_gallery']['#items']) < 1): 
  hide($content['group_tratamiento_tabs']['group_tratamiento_tab_galeria']['field_gallery']); 
  hide($content['group_tratamiento_tabs']['group_tratamiento_tab_galeria']); 
 endif;
  }

  $node->content['myfield'] = array(
    '#markup' => theme('mymodule_myfield', $node->myfield), 
    '#weight' => 1,
  );

  return $node;
}

function icp_theme_breadcrumb($breadcrumb) {
if (!empty($breadcrumb)) {
$breadcrumb ["breadcrumb"][] = t(drupal_get_title());
return '<div class="breadcrumb">' . implode(' » ', $breadcrumb["breadcrumb"]) . '</div>';

}
}

/**
 * Override or insert variables into the html template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function icp_theme_preprocess_html(&$vars) {
  //Show grid
  $show_grid = theme_get_setting('show_grid');
  if($show_grid){$vars['classes_array'][] = "show-grid";};

  //Active Responsive
  $vars['active_responsive'] = theme_get_setting('active_responsive');

  //Responsive menu tree
  $main_menu = menu_tree('main-menu');
  $tablet_menu = menu_tree('menu-tablet-menu');
  $mobile_menu = menu_tree('menu-mobile-menu');
  $responsive_menu = menu_tree('menu-responsive-menu');
  $menu_add_endresponsivemenu = menu_tree('menu-secundario');

  if($tablet_menu && $mobile_menu){
    //PINTA MENU PARA TABLET Y PARA MOBILE
    $pr1 = drupal_render($tablet_menu);
    $pr2 = drupal_render($mobile_menu);

    $pr1 = str_replace(' class="menu"', ' class="responsive tablet"', $pr1);
    $pr2 = str_replace(' class="menu"', ' class="responsive mobile"', $pr2);

    $pr = $pr1 . $pr2;

    $pr = $pr . drupal_render($menu_add_endresponsivemenu);
    $pr = str_replace(' class="menu"', ' class="secondary-responsive"', $pr);
    
    $render = $pr;
  } else if($responsive_menu) {
    //PINTA MENU RESPONSIVE
    $pr = drupal_render($responsive_menu);
    $pr = str_replace(' class="menu"', ' class="responsive"', $pr);

    $pr = $pr . drupal_render($menu_add_endresponsivemenu);
    $pr = str_replace(' class="menu"', ' class="secondary-responsive"', $pr);

    $render = $pr;
  } else {
    //PINTA MENU PRINCIPAL
    $pr = drupal_render($main_menu);
    $pr = str_replace(' class="menu"', ' class="responsive"', $pr);

    $pr = $pr . drupal_render($menu_add_endresponsivemenu);
    $pr = str_replace(' class="menu"', ' class="secondary-responsive"', $pr);

    $render = $pr;
  }

  $vars['main_menu_responsive_panel'] = $render;
  

  //is home? is logged?
  $vars['is_home']        = drupal_is_front_page();
  $vars['is_logged']      = user_is_logged_in();
}

/**
 * Override or insert variables into the page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function icp_theme_preprocess_page(&$vars) { 
  //Settings to JS
  $vars['active_responsive'] = theme_get_setting('active_responsive');
  drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . path_to_theme() . '" });', 'inline');
  drupal_add_js('jQuery.extend(Drupal.settings, { "activeResponsive": "' . $vars['active_responsive'] . '" });', 'inline');
}
 